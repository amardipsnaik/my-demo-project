stages:
  - Plan
  - Deploy

PlanDeployOnDev:
  stage: Plan
  image: $CI_REGISTRY/library/docker/terraform
  script:
    - echo "Terraform planning for EC2 Instance Ceation.."
    - cd ./devops
    - terraform init
    - terraform plan 

DeployDev:
  stage: Deploy
  image: $CI_REGISTRY/library/docker/terraform
  script:
    - chmod 777 ./devops/ec2Stack.tf
    - echo "Applying Changes on EC2"
    - cd ./devops
    - terraform init 
    - terraform plan 
    - terraform apply 